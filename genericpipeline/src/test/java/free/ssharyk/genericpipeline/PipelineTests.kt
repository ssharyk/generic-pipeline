package free.ssharyk.genericpipeline

import org.junit.Assert
import org.junit.Test

class PipelineTest {

    @Test
    fun whenMakePipelineAsVariable_thenResultProvided() {

        val pipeline = pipeline<Int, Float> {

            step {
                it.toString().repeat(it)
            }.then {
                it.toLong()
            }.then {
                -2f * it
            }
        }


        val result = pipeline(4)

        Assert.assertEquals(-8888f, result)
    }

    @Test
    fun whenMakeAndRunPipeline_thenResultProvided() {

        val result = pipeline<Int, Float>(4) {

            step {
                it.toString().repeat(it)
            } then {
                it.toLong()
            } then {
                -2f * it
            }
        }

        Assert.assertEquals(-8888f, result)
    }
}
