package free.ssharyk.genericpipeline

/**
 * Represents a generic pipelines that passes single value of type [TIn],
 * executes serial steps one-by-one, using result of passed step as an input
 * for the next one and finally returns result of type [TOut]
 */
class Pipeline<TIn, TOut> {

    private val steps = mutableListOf<PipelineStep<Any, Any>>()

    /**
     * Creates first step in the pipeline.
     *
     * The input type is defined in the Pipeline declaration;
     * the output type is type of the step itself.
     */
    fun <TOut> step(action: (TIn) -> TOut): PipelineStep<TIn, TOut> {
        val step = PipelineStep<TIn, TOut>(this, action)
        addStep(step)
        return step
    }

    /**
     * Execute all steps in the pipeline
     *
     * @param input Input value to start the pipeline
     * @return Result of the pipeline
     */
    operator fun invoke(input: TIn): TOut {
        var currentRes: Any = input as Any
        for (st in steps) {
            currentRes = st.invoke(currentRes)
        }
        return currentRes as TOut
    }

    internal fun <TStepIn, TStepOut> addStep(step: PipelineStep<TStepIn, TStepOut>) {
        steps.add(step as PipelineStep<Any, Any>)
    }

    /// TODO
    // .complete
    // OR
    // .catch

}
