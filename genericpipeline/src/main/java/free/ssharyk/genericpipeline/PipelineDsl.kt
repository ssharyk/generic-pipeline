package free.ssharyk.genericpipeline

/**
 * DSL-form to create generic pipeline
 *
 * @param initialize Block of steps creation (see [Pipeline.step] to create the first step
 * and [PipelineStep.then] to create all the rest steps until finish)
 *
 * @return Created pipeline
 */
fun <TIn, TOut> pipeline(initialize: Pipeline<TIn, TOut>.() -> Unit): Pipeline<TIn, TOut> {
    return Pipeline<TIn, TOut>().apply(initialize)
}

/**
 * DSL-form to create and immediately run generic pipeline
 *
 * @param input Input value to start the pipeline
 * @param initialize Block of steps creation
 * @return result of Pipeline computation
 */
fun <TIn, TOut> pipeline(input: TIn, initialize: Pipeline<TIn, TOut>.() -> Unit): TOut {
    val pipeline = Pipeline<TIn, TOut>().apply(initialize)
    return pipeline(input)
}
