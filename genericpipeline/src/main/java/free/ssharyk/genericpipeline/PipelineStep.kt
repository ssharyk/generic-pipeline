package free.ssharyk.genericpipeline

/**
 * Represents single operation during the Pipeline.
 *
 * @param pipeline Owner of the step. Once step is created (either via [Pipeline.step] of [PipelineStep.then],
 * it's bounded to the pipeline)
 * @param action Actual operation. The computation is lazy and executed only when [Pipeline.invoke] called.
 */
class PipelineStep<TIn, TOut>(
    private val pipeline: Pipeline<*, *>,
    private val action: (TIn) -> TOut
) {

    /**
     * Executes the step
     */
    operator fun invoke(input: TIn): TOut {
        return action(input)
    }

    /**
     * Create new step, witch has the same input type as the origin step's output
     *
     * @param action Operation of the new step
     */
    infix fun <TNext> then(action: (TOut) -> TNext): PipelineStep<TOut, TNext> {
        val step = PipelineStep(pipeline, action)
        pipeline.addStep(step)
        return step
    }

}
